﻿using System;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Monopoly.Data.Models;

namespace Monopoly.Data
{
    public interface IGameSessionRepository
    {
        GameSession Get(int id);
        GameSession Insert(GameSession gameSession);
        bool Update(GameSession gameSession);
    }
}
