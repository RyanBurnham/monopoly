using System.Collections.Generic;
using System.Linq;
using Monopoly.Data.Models;

namespace Monopoly.Data
{
    public class GameSessionRepository : IGameSessionRepository
    {
        private readonly List<GameSession> _gameSessions = new List<GameSession>();

        public GameSessionRepository()
        {
            //Add mock session
            _gameSessions.Add(new GameSession()
            {
                Id = 1,
                BoardType = BoardType.Classic,
            });
        }
        
        public GameSession Get(int id)
        {
            return _gameSessions.FirstOrDefault(g => g.Id == id);
        }

        public GameSession Insert(GameSession gameSession)
        {
            gameSession.Id = _gameSessions.Count + 1;
            _gameSessions.Add(gameSession);

            return gameSession;
        }

        public bool Update(GameSession gameSession)
        {
            var session = _gameSessions.FirstOrDefault(g => g.Id == gameSession.Id);
            if (session != null)
            {
                session.Players = gameSession.Players;
                session.BoardType = gameSession.BoardType;
            }
            return true;
        }
    }
}