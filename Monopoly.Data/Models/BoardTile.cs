﻿namespace Monopoly.Data.Models
{
    public class BoardTile
    {
        public int Position { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public BoardType BoardType { get; set; }
        public TileType Type { get; set; }
        public bool CanBePurchased { get; set; }
        public string Group { get; set; }
    }

    public enum TileType
    {
        Go,
        Chance,
        Railway,
        Property
    }
}