﻿namespace Monopoly.Data.Models
{
    public class PlayerSessionLandRights
    {
        public int PropertyId { get; set; }
        public int NumberOfBuildings { get; set; }
        public int MemberId { get; set; }
        public int GameSessionId { get; set; }
    }
}