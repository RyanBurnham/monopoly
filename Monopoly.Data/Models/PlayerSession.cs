﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Monopoly.Data.Models
{
    public class PlayerSession
    {
        public int GameSessionId { get; set; }
        public int MemberId { get; set; }
        public List<PlayerSessionLandRights> PlayerSessionLandRights { get; set; }
        public decimal AccountBalance { get; set; }
    }
}
