﻿using System.Collections.Generic;

namespace Monopoly.Data.Models
{
    public class GameSession
    {
        public GameSession()
        {
            Players = new List<Member>();
        }

        public int Id { get; set; }
        public List<Member> Players { get; set; }
        public BoardType BoardType { get; set; }
    }
}
