﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Monopoly.Data.Models
{
    public class Member
    {
        public int MemberId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PasswordHash { get; set; }
        public string PasswordSalt { get; set; }


    }
}
