﻿using System.Collections.Generic;
using Monopoly.Data.Models;

namespace Monopoly.Data
{
    public interface IBoardTileRepository
    {
        IEnumerable<BoardTile> GetBoardTiles(BoardType boardType);
    }
}