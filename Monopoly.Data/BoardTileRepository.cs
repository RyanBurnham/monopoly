﻿using System;
using System.Collections.Generic;
using System.Text;
using Monopoly.Data.Models;

namespace Monopoly.Data
{
    public class BoardTileRepository : IBoardTileRepository
    {
        public IEnumerable<BoardTile> GetBoardTiles(BoardType boardType)
        {
            return new List<BoardTile>()
            {
                new BoardTile()
                {
                    Position = 1,
                    Name = "Go",
                    BoardType = BoardType.Classic,
                    CanBePurchased = false,
                    Type = TileType.Go
                },
                new BoardTile()
                {
                    Position = 2,
                    Name = "Property 1",
                    BoardType = BoardType.Classic,
                    CanBePurchased = false,
                    Price = 100,
                    Group = "Red",
                    Type = TileType.Property

                },
                new BoardTile()
                {
                    Position = 3,
                    Name = "Property 2",
                    BoardType = BoardType.Classic,
                    CanBePurchased = false,
                    Price = 110,
                    Group = "Red",
                    Type = TileType.Property
                },
                new BoardTile()
                {
                    Position = 4,
                    Name = "Chance",
                    BoardType = BoardType.Classic,
                    CanBePurchased = false,
                    Type = TileType.Chance
                },
                new BoardTile()
                {
                    Position = 5,
                    Name = "Property 3",
                    BoardType = BoardType.Classic,
                    CanBePurchased = true,
                    Price = 200,
                    Group = "Blue",
                    Type = TileType.Property
                },
                new BoardTile()
                {
                    Position = 6,
                    Name = "Property 4",
                    BoardType = BoardType.Classic,
                    CanBePurchased = true,
                    Price = 200,
                    Group = "Blue",
                    Type = TileType.Property
                },
                new BoardTile()
                {
                    Position = 7,
                    Name = "Chance",
                    BoardType = BoardType.Classic,
                    CanBePurchased = false,
                    Type = TileType.Chance
                }
            };
        }
    }
}
