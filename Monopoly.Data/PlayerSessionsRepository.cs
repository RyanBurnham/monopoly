﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Monopoly.Data.Models;

namespace Monopoly.Data
{
    public class PlayerSessionsRepository : IPlayerSessionsRepository
    {
        private readonly List<PlayerSession> _playerSessions = new List<PlayerSession>();

        public PlayerSession Get(int gameSessionId, int memberId)
        {
            return _playerSessions.FirstOrDefault(p => p.MemberId == memberId && p.GameSessionId == gameSessionId);
        }

        public bool Insert(PlayerSession playerSession)
        {
            _playerSessions.Add(playerSession);
            return true;
        }

        public bool Update(PlayerSession playerSession)
        {
            var session = _playerSessions.FirstOrDefault(p => p.MemberId == playerSession.MemberId && p.GameSessionId == playerSession.GameSessionId);
            if (session != null)
            {
                session.AccountBalance = playerSession.AccountBalance;
                session.PlayerSessionLandRights = playerSession.PlayerSessionLandRights;
            }
            return true;
        }
    }
}
