﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Monopoly.Data.Models;

namespace Monopoly.Data
{
    public interface IPlayerSessionLandRightsRepositroy
    {
        IList<PlayerSessionLandRights> GetAll(int gameSessionId, int memberId);
        PlayerSessionLandRights Get(int gameSessionId, int memberId, int propertyId);
        bool Insert(PlayerSessionLandRights playerSession);
        bool Update(PlayerSessionLandRights playerSessionLandRights);
    }

    public class PlayerSessionLandRightsRepositroy : IPlayerSessionLandRightsRepositroy
    {
        private readonly List<PlayerSessionLandRights> _playerSessionLandRights = new List<PlayerSessionLandRights>();


        public IList<PlayerSessionLandRights> GetAll(int gameSessionId, int memberId)
        {
            return _playerSessionLandRights.Where(p => p.MemberId == memberId && p.GameSessionId == gameSessionId).ToList();
        }

        public PlayerSessionLandRights Get(int gameSessionId, int memberId, int propertyId)
        {
            return _playerSessionLandRights.FirstOrDefault(p => p.MemberId == memberId && p.GameSessionId == gameSessionId && p.PropertyId == propertyId);
        }

        public bool Insert(PlayerSessionLandRights playerSession)
        {
            _playerSessionLandRights.Add(playerSession);
            return true;
        }

        public bool Update(PlayerSessionLandRights playerSessionLandRights)
        {
            var landRights = _playerSessionLandRights.FirstOrDefault(p => p.MemberId == playerSessionLandRights.MemberId 
            && p.GameSessionId == playerSessionLandRights.GameSessionId 
            && p.PropertyId == playerSessionLandRights.PropertyId);

            if (landRights != null)
            {
                landRights.NumberOfBuildings = playerSessionLandRights.NumberOfBuildings;
            }
            return true;
        }
    }
}
