﻿using Monopoly.Data.Models;

namespace Monopoly.Data
{
    public interface IPlayerSessionsRepository
    {
        PlayerSession Get(int gameSessionId, int memberId);
        bool Insert(PlayerSession playerSession);
        bool Update(PlayerSession playerSession);
    }
}