using Monopoly.Data.Models;

namespace Monopoly.Data
{
    public interface IMemberRepository
    {
        Member GetMember(int id);
    }
}