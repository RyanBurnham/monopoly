# Monopoly

This is a sample project to demonstrate solutions you might see in a real world application. 

It's a work in progress and could still use many improvements. 

#Setup
The project was created using the latest version of Visual Studio 2017 community edition. It's based on the ASP.net core with Angular 2 template.

* Install the latest version of Visual Studio 2017
* Run the project
* Navigate to the Game Board to see a basic implementation of the Game Board and Tile components that are driven from api responses.

###Know issues###
* I've ran into some build errors relating to node-sass.

"Node Sass could not find a binding for your current environment: Windows 64-bit with Node.js 6.x"

To fix run 'npm rebuild node-sass' in the Monopoly directory

##Implemented
* Basic repository system (currently in memory)
* Basic member and game session api's
* Angular components for game board and tiles that loads a game session from the game session api.
* Implemented basic board tiles and provided mock data

##TODO

* Finish implementing the Game Board
* Implement member authentication
* Implement a command system to give the user the ability to send player actions such as roll dice
* Implement a game manager to enforce game rules and coordinate commands with other players
* Implement basic game play mechanics, start game, piece movement.
* Implement data persistence
