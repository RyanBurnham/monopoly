﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Monopoly.Data;
using Monopoly.Data.Models;

namespace Monopoly.Services
{
    public class BoardTilesFactory
    {
        private readonly IBoardTileRepository _boardTileRepository;
        private readonly Dictionary<BoardType, IEnumerable<BoardTile>> _boardTileCache = new Dictionary<BoardType, IEnumerable<BoardTile>>();

        public BoardTilesFactory(IBoardTileRepository boardTileRepository)
        {
            _boardTileRepository = boardTileRepository;
        }
        public IEnumerable<BoardTile> GetBoardTiles(BoardType boardType)
        {
            if (_boardTileCache.ContainsKey(boardType))
                return _boardTileCache[boardType];

            var boardTiles = _boardTileRepository.GetBoardTiles(boardType).ToList();
            _boardTileCache.Add(boardType, boardTiles);
            return boardTiles;
        }
    }
}
