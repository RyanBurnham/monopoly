﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Monopoly.Data.Models;

namespace Monopoly.Models
{
    public class GameSessionDto
    {
        public int Id { get; set; }
        public List<Member> Players { get; set; }
        public BoardType BoardType { get; set; }
        public List<BoardTile> BoardTiles { get; set; }
    }
}

