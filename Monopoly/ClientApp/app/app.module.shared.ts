import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './components/app/app.component';
import { NavMenuComponent } from './components/navmenu/navmenu.component';
import { HomeComponent } from './components/home/home.component';
import { TileComponent } from './components/tile/tile.component';
import { PropertyTileComponent } from './components/property-tile/property-tile.component';
import { GameBoardComponent } from "./components/game-board/game-board.component";
import {GoTileComponent} from "./components/go-tile/go-tile.component";
import {ChanceTileComponent} from "./components/chance-tile/chance-tile.component";

@NgModule({
    declarations: [
        AppComponent,
        NavMenuComponent,
        HomeComponent,
        TileComponent,
        PropertyTileComponent,
        GameBoardComponent,
        GoTileComponent,
        ChanceTileComponent
    ],
    imports: [
        CommonModule,
        HttpModule,
        FormsModule,
        RouterModule.forRoot([
            { path: '', redirectTo: 'home', pathMatch: 'full' },
            { path: 'home', component: HomeComponent },
            { path: 'game-board', component: GameBoardComponent },
            { path: '**', redirectTo: 'home' }
        ])
    ]
})
export class AppModuleShared {
}


