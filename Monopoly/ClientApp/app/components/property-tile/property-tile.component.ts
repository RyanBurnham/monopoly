﻿import { Component, Input } from '@angular/core';
import { IBoardTile } from '../../global/models/domain-models';

@Component({
    selector: 'app-property-tile',
    templateUrl: './property-tile.component.html',
    styleUrls: ['./property-tile.component.scss']
})
/** property-tile component*/
export class PropertyTileComponent {
    /** property-tile ctor */
    constructor() {

    }

    @Input() boardTile: IBoardTile;
}