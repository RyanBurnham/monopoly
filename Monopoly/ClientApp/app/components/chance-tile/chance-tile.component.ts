﻿import { Component, Input } from '@angular/core';
import { IBoardTile } from "../../global/models/domain-models";

@Component({
    selector: 'app-chance-tile',
    templateUrl: './chance-tile.component.html',
    styleUrls: ['./chance-tile.component.scss']
})
/** chance-tile component*/
export class ChanceTileComponent {
    /** chance-tile ctor */
    constructor() {

    }

    @Input() boardTile: IBoardTile;
}