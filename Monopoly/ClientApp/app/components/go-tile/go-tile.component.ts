﻿import { Component, Input } from '@angular/core';
import { IBoardTile } from "../../global/models/domain-models";

@Component({
    selector: 'app-go-tile',
    templateUrl: './go-tile.component.html',
    styleUrls: ['./go-tile.component.scss']
})
/** go-tile component*/
export class GoTileComponent {
    /** go-tile ctor */
    constructor() {

    }

    @Input() boardTile: IBoardTile;
}