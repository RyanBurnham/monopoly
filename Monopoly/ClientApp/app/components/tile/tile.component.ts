﻿import { Component, Input } from '@angular/core';
import { IBoardTile } from '../../global/models/domain-models';


@Component({
    selector: 'app-tile',
    templateUrl: './tile.component.html',
    styleUrls: ['./tile.component.scss']
})
/** tile component*/
export class TileComponent {
    /** tile ctor */
    constructor() {

    }

    @Input() public boardTile: IBoardTile;

    getSizeClass(tileType: string) {
        switch (tileType) {
            case "Go": return "two-by-two";
            default :
                return "one-by-two";
        }
    }
}