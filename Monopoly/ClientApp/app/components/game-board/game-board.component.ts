﻿import { Component, Inject } from '@angular/core';
import { Http } from '@angular/http';
import { IGameSession } from '../../global/models/domain-models';

@Component({
    selector: 'app-game-board',
    templateUrl: './game-board.component.html',
    styleUrls: ['./game-board.component.scss']
})
/** game-board component*/
export class GameBoardComponent {
    public gameSession: IGameSession;

    /** game-board ctor */
    constructor(http: Http, @Inject('BASE_URL') baseUrl: string) {

        //TODO: implement proper service for loading of data
        http.get(baseUrl + 'api/GameSession/1').subscribe(result => {
            this.gameSession = result.json() as IGameSession;
        }, error => console.error(error));
    }
}