﻿export interface IGameSession {
    id: number,
    players: Array<IMember>,
    boardType: number,
    boardTiles: Array<IBoardTile>,
}


export interface IBoardTile {
    position: number,
    name: string,
    price: number,
    boardType: string,
    type: string,
    canBePurchased: boolean,
    group: string,
}

export interface IMember {

}
