using System;
using System.Collections.Generic;
using System.ComponentModel.Design.Serialization;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Routing.Constraints;
using Microsoft.AspNetCore.SpaServices.Webpack;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Monopoly.Data;
using Monopoly.Data.Models;
using Monopoly.Models;
using Monopoly.Services;
using StackExchange.Redis;

namespace Monopoly
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().AddJsonOptions(options =>
            {
                options.SerializerSettings.Converters.Add(new Newtonsoft.Json.Converters.StringEnumConverter());
                options.SerializerSettings.NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore;
            });
            services.AddTransient<IMemberRepository, MemberRepository>();
            services.AddTransient<IGameSessionRepository, GameSessionRepository>();
            services.AddTransient<IPlayerSessionsRepository,PlayerSessionsRepository>();
            services.AddTransient<IPlayerSessionLandRightsRepositroy, PlayerSessionLandRightsRepositroy>();
            services.AddTransient<IBoardTileRepository, BoardTileRepository>();
            services.AddSingleton<BoardTilesFactory>();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseWebpackDevMiddleware(new WebpackDevMiddlewareOptions
                {
                    HotModuleReplacement = true
                });
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
                routes.MapSpaFallbackRoute(
                    name: "spa-fallback",
                    defaults: new {controller = "Home", action = "Index"}
                    );
            });

            ConfigureMappings(app, env);
        }

        private void ConfigureMappings(IApplicationBuilder app, IHostingEnvironment env)
        {
            Mapper.Initialize(config =>
            {
                config.CreateMap<GameSession, GameSessionDto>();

            });
        }
    }
}
