﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Monopoly.Data;
using Monopoly.Data.Models;
using Monopoly.Models;
using Monopoly.Services;

namespace Monopoly.Api
{
    [Produces("application/json")]
    [Route("api/GameSession")]
    public class GameSessionController : Controller
    {
        private readonly BoardTilesFactory _gameSessionFactory;
        private readonly IGameSessionRepository _gameSessionRepository;

        public GameSessionController(BoardTilesFactory gameSessionFactory, IGameSessionRepository gameSessionRepository)
        {
            _gameSessionFactory = gameSessionFactory;
            _gameSessionRepository = gameSessionRepository;
        }

        // GET: api/GameSession/5
        [HttpGet("{id}")]
        public GameSessionDto Get(int id)
        {
            var session = _gameSessionRepository.Get(id);
            
            return MapToDto(session);
        }
        
        // POST: api/GameSession
        [HttpPost]
        public GameSessionDto Post(BoardType boardType)
        {
            //TODO fix model binding for boardType
            var session = _gameSessionRepository.Insert(new GameSession() { BoardType = boardType });
            return MapToDto(session);
        }

        private GameSessionDto MapToDto(GameSession session)
        {
            var dto = Mapper.Map<GameSession, GameSessionDto>(session);
            dto.BoardTiles = _gameSessionFactory.GetBoardTiles(session.BoardType).ToList();
            return dto;
        }
        
    }
}
