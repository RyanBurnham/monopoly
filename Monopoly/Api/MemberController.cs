﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Monopoly.Data;
using Monopoly.Data.Models;

namespace Monopoly.Api
{
    [Produces("application/json")]
    [Route("api/Member")]
    public class MemberController : Controller
    {
        private readonly IMemberRepository _memberRepository;

        public MemberController(IMemberRepository memberRepository)
        {
            _memberRepository = memberRepository;
        }
        
        // GET: api/Member/5
        [HttpGet("{id}")]
        public Member Get(int id)
        {
             return _memberRepository.GetMember(id);
        }
    }
}
